<?php
// 本类由系统自动生成，仅供测试用途
namespace Home\Controller;
class IndexController extends HomeController {
    public function index(){
	    //js.html 地址
		$qrjs_url = SITE_URL;
		//$qrjs_url .= '/js.html?';  //支持  rewrite 
		$qrjs_url .= '/index.php?s=js.html&'; // 不支持  rewrite 


		//缓存统计
		$count = S('qrcode_count');
		
		if(!$count)
		{
			$count = D('qrcode')->count();
			$count = S('qrcode_count',$count,86400);//缓存1天
		}

		

		$this->assign('qrjs_url',$qrjs_url);
		$this->assign('count',$count);
        $this->display();
    }
    
  
    //测试二维码
    public function qrtest()
    {
        import('@.Org.QRcode');
        //tel:13724026923
        //url:http://www.leipi.org
        //test text
        \QRcode::svn('url:http://www.leipi.org', './test.png', 'L',4, 2);
         echo '<img src="/test.png" /><hr/>';  
    }
}